msgid ""
msgstr ""
"Project-Id-Version: plasma_engine_mpris2\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-15 00:20+0000\n"
"PO-Revision-Date: 2012-05-12 14:49-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: multiplexedservice.cpp:68
#, kde-format
msgctxt "Name for global shortcuts category"
msgid "Media Controller"
msgstr ""

#: multiplexedservice.cpp:70
#, kde-format
msgid "Play/Pause media playback"
msgstr ""

#: multiplexedservice.cpp:88
#, kde-format
msgid "Media playback next"
msgstr ""

#: multiplexedservice.cpp:97
#, kde-format
msgid "Media playback previous"
msgstr ""

#: multiplexedservice.cpp:106
#, kde-format
msgid "Stop media playback"
msgstr ""

#: multiplexedservice.cpp:115
#, kde-format
msgid "Pause media playback"
msgstr ""

#: multiplexedservice.cpp:124
#, kde-format
msgid "Play media playback"
msgstr ""

#: multiplexedservice.cpp:133
#, kde-format
msgid "Media volume up"
msgstr ""

#: multiplexedservice.cpp:142
#, kde-format
msgid "Media volume down"
msgstr ""

#: playeractionjob.cpp:169
#, kde-format
msgid "The media player '%1' cannot perform the action '%2'."
msgstr ""

#: playeractionjob.cpp:171
#, kde-format
msgid "Attempting to perform the action '%1' failed with the message '%2'."
msgstr ""

#: playeractionjob.cpp:173
#, kde-format
msgid "The argument '%1' for the action '%2' is missing or of the wrong type."
msgstr ""

#: playeractionjob.cpp:175
#, kde-format
msgid "The operation '%1' is unknown."
msgstr ""

#: playeractionjob.cpp:177
#, kde-format
msgid "Unknown error."
msgstr ""
